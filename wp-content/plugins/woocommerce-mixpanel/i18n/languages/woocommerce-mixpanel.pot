# Copyright (C) 2017 SkyVerge
# This file is distributed under the GNU General Public License v3.0.
msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Mixpanel 1.11.0\n"
"Report-Msgid-Bugs-To: https://woocommerce.com/my-account/tickets/\n"
"POT-Creation-Date: 2014-06-15 22:41:28+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2017-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"

#: includes/class-wc-mixpanel-integration.php:67
msgid "Mixpanel"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:68
msgid ""
"Real time Web analytics tool that tracks visitors to your site as people, "
"not pageviews. Visualize your online sales funnels and find out which ones "
"are driving revenue and which are not."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1175
msgid "API Settings"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1176
msgid "Enter your project token to start tracking."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1182
msgid "Token"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1183
msgid ""
"Log into your account and go to Update Project settings. Leave blank to "
"disable tracking."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1189
msgid "Nametag Preference"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1190
msgid "Select how to identify logged in users in the Stream View."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1194
msgid "Email Address"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1195
msgid "WordPress Username"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1200
msgid "Logging"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1201
msgid ""
"Select whether to log nothing, queries, errors, or both queries and errors "
"to the WooCommerce log. Careful, this can fill up log files very quickly on "
"a busy site."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1205
msgid "Off"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1206
msgid "Queries"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1207
msgid "Errors"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1208
msgid "Queries & Errors"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1213
msgid "Geocode IPs"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1214
msgid "Allow Mixpanel to geolocate users."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1220
msgid "Event Names"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1221
msgid ""
"Customize the event names. Leave a field blank to disable tracking of that "
"event."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1227
msgid "Signed In"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1228
msgid "Triggered when a customer signs in."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1234
msgid "Signed Out"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1235
msgid "Triggered when a customer signs out."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1241
msgid "Viewed Signup"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1242
msgid "Triggered when a customer views the registration form."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1248
msgid "Signed Up"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1249
msgid "Triggered when a customer registers a new account."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1255
msgid "Viewed Homepage"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1256
msgid "Triggered when a customer views the homepage."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1262
msgid "Viewed Product"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1263
msgid "Triggered when a customer views a single product"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1269
msgid "Added to Cart"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1270
msgid "Triggered when a customer adds an item to the cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1276
msgid "Removed from Cart"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1277
msgid "Triggered when a customer removes an item from the cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1283
msgid "Changed Cart Quantity"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1284
msgid "Triggered when a customer changes the quantity of an item in the cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1290
msgid "Viewed Cart"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1291
msgid "Triggered when a customer views the cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1297
msgid "Applied Coupon"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1298
msgid "Triggered when a customer applies a coupon"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1304
msgid "Started Checkout"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1305
msgid "Triggered when a customer starts the checkout."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1311
msgid "Started Payment"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1312
msgid ""
"Triggered when a customer views the payment page (used with direct post "
"payment gateways)"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1318
msgid "Completed Purchase"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1319
msgid "Triggered when a customer completes a purchase."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1325
msgid "Completed Payment"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1326
msgid "Triggered when a customer completes payment for their purchase."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1332
msgid "Wrote Review"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1333
msgid "Triggered when a customer writes a review."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1339
msgid "Commented"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1340
msgid "Triggered when a customer write a comment."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1346
msgid "Viewed Account"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1347
msgid "Triggered when a customer views the My Account page."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1353
msgid "Viewed Order"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1354
msgid "Triggered when a customer views an order"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1360
msgid "Updated Address"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1361
msgid "Triggered when a customer updates their address."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1367
msgid "Changed Password"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1368
msgid "Triggered when a customer changes their password."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1374
msgid "Estimated Shipping"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1375
msgid "Triggered when a customer estimates shipping."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1381
msgid "Tracked Order"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1382
msgid "Triggered when a customer tracks an order."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1388
msgid "Cancelled Order"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1389
msgid "Triggered when a customer cancels an order."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1395
msgid "Reordered"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1396
msgid "Triggered when a customer reorders a previous order."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1402
msgid "Property Names"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1403
msgid ""
"Customize the property names. Leave a field blank to disable tracking of "
"that property."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1409
msgid "Product Name"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1410
msgid ""
"Tracked when a customer views a product, adds / removes / changes "
"quantities in the cart, or writes a review."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1416
msgid "Product Price"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1417
msgid "Tracked when a customer adds a product to the cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1423
msgid "Product Quantity"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1424
msgid ""
"Tracked when a customer adds a product to their cart or changes the "
"quantity in their cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1430
msgid "Product Category"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1431
msgid "Tracked when a customer adds a product to their cart."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1437
msgid "Coupon Code"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1438
msgid "Tracked when a customer applies a coupon."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1444
msgid "Order ID"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1445
#: includes/class-wc-mixpanel-integration.php:1452
#: includes/class-wc-mixpanel-integration.php:1459
#: includes/class-wc-mixpanel-integration.php:1466
#: includes/class-wc-mixpanel-integration.php:1473
msgid "Tracked when a customer completes their purchase."
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1451
msgid "Order Total"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1458
msgid "Shipping Total"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1465
msgid "Total Quantity"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1472
msgid "Payment Method"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1479
msgid "Post Title"
msgstr ""

#: includes/class-wc-mixpanel-integration.php:1480
msgid "Tracked when a customer leaves a comment on a post."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:69
#. translators: Placeholders: %1$s - <a> tag, %2$s - </a> tag
msgid ""
"Please %1$supdate%2$s your Mixpanel settings in order to start tracking "
"Subscription events."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:629
msgid "Subscription Event Names"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:630
msgid ""
"Customize the event names for Subscription events. Leave a field blank to "
"disable tracking of that event."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:634
msgid "Activated Subscription"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:635
msgid "Triggered when a customer activates their subscription."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:640
msgid "Subscription Free Trial Ended"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:641
msgid "Triggered when a the free trial ends for a subscription."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:646
msgid "Subscription End of Pre-Paid Term"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:647
msgid ""
"Triggered when the end of a pre-paid term for a previously cancelled "
"subscription is reached."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:652
msgid "Subscription Expired"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:653
msgid "Triggered when a subscription expires."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:658
msgid "Suspended Subscription"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:659
msgid "Triggered when a customer suspends their subscription."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:664
msgid "Reactivated Subscription"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:665
msgid "Triggered when a customer reactivates their subscription."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:670
msgid "Cancelled Subscription"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:671
msgid "Triggered when a customer cancels their subscription."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:676
msgid "Renewed Subscription"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:677
msgid ""
"Triggered when a customer is automatically billed for a subscription "
"renewal."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:682
msgid "Subscription Property Names"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:683
msgid ""
"Customize the property names for Subscription events. Leave a field blank "
"to disable tracking of that property."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:687
msgid "Subscription Name"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:688
msgid "Tracked anytime a subscription event occurs."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:693
msgid "Subscription ID"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:694
#: includes/class-wc-mixpanel-subscriptions-integration.php:700
msgid "Tracked when a subscription is activated."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:699
msgid "Subscription Price"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:705
msgid "Total Initial Payment"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:706
msgid ""
"Tracked for subscription activations. Includes the Recurring amount and "
"Sign Up Fee."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:711
msgid "Initial Sign Up Fee"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:712
msgid ""
"Tracked for subscription activations. This will be zero if the subscription "
"has no sign up fee."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:717
msgid "Subscription Period"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:718
msgid "Tracks the period (e.g. Day, Month, Year) for subscription activations."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:723
msgid "Subscription Interval"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:724
msgid ""
"Tracks the interval (e.g. every 1st, 2nd, 3rd, etc.) for subscription "
"activations."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:729
msgid "Subscription Length"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:730
msgid ""
"Tracks the length (e.g. infinite, 12 months, 2 years, etc.) for "
"subscription activations."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:735
msgid "Subscription Trial Period"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:736
msgid ""
"Tracks the trial period (e.g. Day, Month, Year) for subscription "
"activations with a free trial."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:741
msgid "Subscription Trial Length"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:742
msgid ""
"Tracks the trial length (e.g. 1-90 periods) for subscription activations "
"with a free trial."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:747
msgid "Billing Amount for Subscription Renewal"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:748
msgid ""
"Tracks the amount billed to the customer when their subscription "
"automatically renews."
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:753
msgid "Billing Description for Subscription Renewal"
msgstr ""

#: includes/class-wc-mixpanel-subscriptions-integration.php:754
msgid ""
"Tracks the name of the subscription billed to the customer when the "
"subscription automatically renews."
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WooCommerce Mixpanel"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.woocommerce.com/products/mixpanel/"
msgstr ""

#. Description of the plugin/theme
msgid "Adds Mixpanel tracking to WooCommerce with one click!"
msgstr ""

#. Author of the plugin/theme
msgid "SkyVerge"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.woocommerce.com"
msgstr ""