<?php

// BEGIN iThemes Security - No modifiques ni borres esta línea
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Desactivar editor de archivos - Seguridad > Ajustes > Ajustes WordPress > Editor de archivos
// END iThemes Security - No modifiques ni borres esta línea

/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('WP_MEMORY_LIMIT', '512M');

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'aremko_wp');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '4R3MK0_R00T');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

define('DISALLOW_FILE_EDIT', true);

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'JaELy;.*fCd-b!+0=47?-6+wJGvppz[|(0mcHkF*ElMp[Gti;xfbx> I~4|-q=g8');
define('SECURE_AUTH_KEY', 'C3-:F-+l;%IEE>1yg[240g z3rK+71l*7S*+y3x==*HW8Pe=*WkbM^ud=8 ^neZE');
define('LOGGED_IN_KEY', 'Vv V.T(EX|Vh?;--<pa51#4poZ>hvk-)T^[Y>Pp)~;T2(>EH8BqdzWz}STw+wi0a');
define('NONCE_KEY', 'c*-qPNGS=9,u8xa:h=@4XyjXCClUf~Vf,worom:iikGMR[N|p8{Q#d_Oe*hXb%fV');
define('AUTH_SALT', '& )J5t)/h0bi6QD?IjUwR%-W{cg~Vi;5F?br[-l7,mN`[Jdg|m8;Gz/0zo;rSt?x');
define('SECURE_AUTH_SALT', 'k)1n5T(&Vv[4n[tTi@uI1]e8YS;~d@wnBD9C%`_GDT+ey&H8J*%ufWb/?)Lc?+-v');
define('LOGGED_IN_SALT', 'VpUy|6lP@7la! mYNr~`FF;ZJ-|=(4<oAui8H!?Y57diE0d+R]:m+S6{)T?O_iJ{');
define('NONCE_SALT', 'XA~4AB^yPEEI0q!&g.~)qeR}?eY2wPzRk~3aE*Q4g-!dMw7L-F7+p0d|%j<xm&rl');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
